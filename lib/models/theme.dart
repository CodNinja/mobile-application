import 'package:flutter/material.dart';

class MyColors {
  static const Color primaryColor = Color(0xFF503E9D);
  static const Color primaryColorLight = Color(0xFF6252A7);
  Map<int, Color> color = {
    50: Color.fromRGBO(80, 64, 157, .1),
    100: Color.fromRGBO(80, 64, 157, .2),
    200: Color.fromRGBO(80, 64, 157, .3),
    300: Color.fromRGBO(80, 64, 157, .4),
    400: Color.fromRGBO(80, 64, 157, .5),
    500: Color.fromRGBO(80, 64, 157, .6),
    600: Color.fromRGBO(80, 64, 157, .7),
    700: Color.fromRGBO(80, 64, 157, .8),
    800: Color.fromRGBO(80, 64, 157, .9),
    900: Color.fromRGBO(80, 64, 157, 1),
  };
}
