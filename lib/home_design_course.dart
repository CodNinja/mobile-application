import 'bottom_nav/bottom_bar_view.dart';
import 'course_info_screen.dart';
// import 'models/fintness_app_theme.dart';
import 'fintness_app_theme.dart';
import 'models/tabIcon_data.dart';
import 'my_diary/my_diary_screen.dart';
import 'pages/training_screen.dart';
import 'popular_course_list_view.dart';
import 'package:flutter/material.dart';
import 'design_course_app_theme.dart';

class DesignCourseHomeScreen extends StatefulWidget {
  @override
  _DesignCourseHomeScreenState createState() => _DesignCourseHomeScreenState();
}

class _DesignCourseHomeScreenState extends State<DesignCourseHomeScreen>
    with TickerProviderStateMixin {
  CategoryType categoryType = CategoryType.ui;
  AnimationController animationController;

  List<TabIconData> tabIconsList = TabIconData.tabIconsList;
  Widget tabBody = Container(
    color: FitnessAppTheme.background,
  );

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    animationController = AnimationController(
        duration: const Duration(milliseconds: 600), vsync: this);
    tabBody = MyDiaryScreen(
      animationController: animationController,
    );
  }

    @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }




    Widget build(BuildContext context) {
    return Container(
      color: FitnessAppTheme.background,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Stack(
                children: <Widget>[
                  tabBody,
                  bottomBar(),
                ],
              )
           
    ));
  }

 

  // @override
  // Widget build(BuildContext context) {
  //   return Container(
  //     color: FitnessAppTheme.background,
  //     child: Scaffold(
  //         backgroundColor: Colors.transparent,
  //         bottomNavigationBar: bottomBar(),
  //         body: Stack(
  //           children: <Widget>[
  //             tabBody,
  //           ],
  //         )),
  //   );
  // }

  // @override
  // Widget build(BuildContext context) {
  //   return Container(
  //     color: DesignCourseAppTheme.nearlyWhite,
  //     child: Scaffold(
  //       bottomNavigationBar: bottomBar(),
  //       backgroundColor: Colors.transparent,
  //       body: Column(
  //         children: <Widget>[
  //           // SizedBox(
  //           //   height: MediaQuery.of(context).padding.top,
  //           // ),
  //           AppBar(
  //             leading: IconButton(
  //         icon: Container(child: Icon(
  //             Icons.arrow_back_ios,
  //             color: MyColors.primaryColor,
  //             size: 16,
  //           ),
  //         ),
  //         onPressed: () => Navigator.of(context).pop(),
  //       ),
  //             elevation: 0,
  //             backgroundColor: Colors.white70,
  //             title: Text(
  //               'Home',
  //               style: TextStyle(color: MyColors.primaryColor),
  //             ),
  //           ),

  //           // getAppBarUI(),
  //           Expanded(
  //             child: SingleChildScrollView(
  //               child: Container(
  //                 height: MediaQuery.of(context).size.height,
  //                 child: Column(
  //                   children: <Widget>[
  //                     // getSearchBarUI(),
  //                     // getCategoryUI(),
  //                     Flexible(
  //                       child: getPopularCourseUI(),
  //                     ),
  //                   ],
  //                 ),
  //               ),
  //             ),
  //           ),
  //         ],
  //       ),
  //     ),
  //   );
  // }

  // Widget getCategoryUI() {
  //   return Column(
  //   mainAxisAlignment: MainAxisAlignment.center,
  //   crossAxisAlignment: CrossAxisAlignment.start,
  //   children: <Widget>[
  // Padding(
  //   padding: const EdgeInsets.only(top: 8.0, left: 18, right: 16),
  //   child: Text(
  //     'Category',
  //     textAlign: TextAlign.left,
  //     style: TextStyle(
  //       fontWeight: FontWeight.w600,
  //       fontSize: 22,
  //       letterSpacing: 0.27,
  //       color: DesignCourseAppTheme.darkerText,
  //     ),
  //   ),
  // ),
  // const SizedBox(
  //   height: 16,
  // ),
  // Padding(
  //   padding: const EdgeInsets.only(left: 16, right: 16),
  //   child: Row(
  //     children: <Widget>[
  //       getButtonUI(CategoryType.ui, categoryType == CategoryType.ui),
  //       const SizedBox(
  //         width: 16,
  //       ),
  //       getButtonUI(
  //           CategoryType.coding, categoryType == CategoryType.coding),
  //       const SizedBox(
  //         width: 16,
  //       ),
  //       getButtonUI(
  //           CategoryType.basic, categoryType == CategoryType.basic),
  //     ],
  //   ),
  // ),
  // const SizedBox(
  //   height: 16,
  // ),
  // CategoryListView(
  //   callBack: () {
  //     moveTo();
  //   },
  // ),

  //   );
  // }

  Widget getPopularCourseUI() {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, left: 18, right: 16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Services',
            textAlign: TextAlign.left,
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 22,
              letterSpacing: 0.27,
              color: DesignCourseAppTheme.darkerText,
            ),
          ),
          Flexible(
            child: PopularCourseListView(
              callBack: () {
                moveTo();
              },
            ),
          )
        ],
      ),
    );
  }

  void moveTo() {
    Navigator.push<dynamic>(
      context,
      MaterialPageRoute<dynamic>(
        builder: (BuildContext context) => CourseInfoScreen(),
      ),
    );
  }

  // Widget getButtonUI(CategoryType categoryTypeData, bool isSelected) {
  //   String txt = '';
  //   if (CategoryType.ui == categoryTypeData) {
  //     txt = 'Ui/Ux';
  //   } else if (CategoryType.coding == categoryTypeData) {
  //     txt = 'Coding';
  //   } else if (CategoryType.basic == categoryTypeData) {
  //     txt = 'Basic UI';
  //   }
  //   return Expanded(
  //     child: Container(
  //       decoration: BoxDecoration(
  //           color: isSelected
  //               ? DesignCourseAppTheme.nearlyBlue
  //               : DesignCourseAppTheme.nearlyWhite,
  //           borderRadius: const BorderRadius.all(Radius.circular(24.0)),
  //           border: Border.all(color: DesignCourseAppTheme.nearlyBlue)),
  //       child: Material(
  //         color: Colors.transparent,
  //         child: InkWell(
  //           splashColor: Colors.white24,
  //           borderRadius: const BorderRadius.all(Radius.circular(24.0)),
  //           onTap: () {
  //             setState(() {
  //               categoryType = categoryTypeData;
  //             });
  //           },
  //           child: Padding(
  //             padding: const EdgeInsets.only(
  //                 top: 12, bottom: 12, left: 18, right: 18),
  //             child: Center(
  //               child: Text(
  //                 txt,
  //                 textAlign: TextAlign.left,
  //                 style: TextStyle(
  //                   fontWeight: FontWeight.w600,
  //                   fontSize: 12,
  //                   letterSpacing: 0.27,
  //                   color: isSelected
  //                       ? DesignCourseAppTheme.nearlyWhite
  //                       : DesignCourseAppTheme.nearlyBlue,
  //                 ),
  //               ),
  //             ),
  //           ),
  //         ),
  //       ),
  //     ),
  //   );
  // }

  // Widget getSearchBarUI() {
  //   return Padding(
  //     padding: const EdgeInsets.only(top: 8.0, left: 18),
  //     child: Row(
  //       mainAxisAlignment: MainAxisAlignment.center,
  //       crossAxisAlignment: CrossAxisAlignment.start,
  //       children: <Widget>[
  //         Container(
  //           width: MediaQuery.of(context).size.width * 0.75,
  //           height: 64,
  //           child: Padding(
  //             padding: const EdgeInsets.only(top: 8, bottom: 8),
  //             child: Container(
  //               decoration: BoxDecoration(
  //                 color: HexColor('#F8FAFB'),
  //                 borderRadius: const BorderRadius.only(
  //                   bottomRight: Radius.circular(13.0),
  //                   bottomLeft: Radius.circular(13.0),
  //                   topLeft: Radius.circular(13.0),
  //                   topRight: Radius.circular(13.0),
  //                 ),
  //               ),
  //               child: Row(
  //                 children: <Widget>[
  //                   Expanded(
  //                     child: Container(
  //                       padding: const EdgeInsets.only(left: 16, right: 16),
  //                       child: TextFormField(
  //                         style: TextStyle(
  //                           fontFamily: 'WorkSans',
  //                           fontWeight: FontWeight.bold,
  //                           fontSize: 16,
  //                           color: DesignCourseAppTheme.nearlyBlue,
  //                         ),
  //                         keyboardType: TextInputType.text,
  //                         decoration: InputDecoration(
  //                           labelText: 'Search for course',
  //                           border: InputBorder.none,
  //                           helperStyle: TextStyle(
  //                             fontWeight: FontWeight.bold,
  //                             fontSize: 16,
  //                             color: HexColor('#B9BABC'),
  //                           ),
  //                           labelStyle: TextStyle(
  //                             fontWeight: FontWeight.w600,
  //                             fontSize: 16,
  //                             letterSpacing: 0.2,
  //                             color: HexColor('#B9BABC'),
  //                           ),
  //                         ),
  //                         onEditingComplete: () {},
  //                       ),
  //                     ),
  //                   ),
  //                   SizedBox(
  //                     width: 60,
  //                     height: 60,
  //                     child: Icon(Icons.search, color: HexColor('#B9BABC')),
  //                   )
  //                 ],
  //               ),
  //             ),
  //           ),
  //         ),
  //         const Expanded(
  //           child: SizedBox(),
  //         )
  //       ],
  //     ),
  //   );
  // }



  Widget bottomBar() {
    return Column(
      children: <Widget>[
        const Expanded(
          child: SizedBox(),
        ),
        BottomBarView(
          tabIconsList: tabIconsList,
          addClick: () {},
          changeIndex: (int index) {
            if (index == 0 || index == 2) {
              animationController.reverse().then<dynamic>((data) {
                if (!mounted) {
                  return;
                }
                setState(() {
                  tabBody =
                      MyDiaryScreen(animationController: animationController);
                });
              });
            } else if (index == 1 || index == 3) {
              animationController.reverse().then<dynamic>((data) {
                if (!mounted) {
                  return;
                }
                setState(() {
                  tabBody =
                      TrainingScreen(animationController: animationController);
                });
              });
            }
          },
        ),
      ],
    );
  }
}

enum CategoryType {
  ui,
  coding,
  basic,
}
